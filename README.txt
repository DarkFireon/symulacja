Projekt symulacji odziału z okienkami (np poczta,bank itp) wraz z wizualizacja.


Oddział posiada N okienek obsługowi charakteryzujących się losowym czasem obsługi o zadanym rozkładzie. Do oddziału przybywają klienci o
losowych odstępach czasu (zadane rozkłady). Klienci charakteryzują się różnymi priorytetami kolejności
obsługi. W oddziale może przebywać jednocześnie ograniczona liczba klientów. W losowych chwilach
pojawiają się awarie sprzętu komputerowego powodujące zamknięcia okienka na pewien losowy czas
(naprawa komputera). Obsługiwany przy nim klient jest przekazywany do innego okienka i czeka na
obsługę w kolejce technicznej (z najwyższym priorytetem). Po obsłudze w danym okienku klient wraca
do kolejki z prawdopodobieństwem p. Każdy klient jest niecierpliwy i po upływie określonego
(losowego) czasu oczekiwania na rozpoczęcie obsługi rezygnuje z niej i wychodzi z banku. Rezygnacja
może nastąpić w trakcie czekania w kolejce jak również podczas samej obsługi przy okienku.

Oszacowane są nastepujace charakterystyki
- oczekiwaną graniczną liczbę klientów w oddziale oraz w kolejce,
- oczekiwany graniczny czas obsługi klienta,
- oczekiwany graniczny czas oczekiwania klienta w kolejce na rozpoczęcie obsługi,
- graniczne prawdopodobieństwo rezygnacji z obsługi przez klienta.
