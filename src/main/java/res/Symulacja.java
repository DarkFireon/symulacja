package res;

import dissimlab.monitors.Diagram;
import dissimlab.monitors.Statistics;
import dissimlab.simcore.SimControlException;
import dissimlab.simcore.SimManager;
import dissimlab.simcore.SimParameters;
import graphics.Screen;
import obiekty.Bank;
import obiekty.Otoczenie;
import wydarzenia.AwariaOkienka;
import wydarzenia.PrzybycieKlienta;

import javax.swing.*;
import java.awt.*;

public class Symulacja implements Runnable {
   private static SimManager simManager;
    private Bank bank;

    public void start(Otoczenie otoczenie,double spowolnienie){
         simManager = new SimManager();
         bank = new Bank(otoczenie,simManager);
        Screen screen= new Screen(bank,1500,800);

        JFrame frame = new JFrame("Symulacja");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(screen);
        frame.pack();
        frame.setVisible(true);
        screen.init();
        screen.addNotify();
            try {
            for(int i=0;i<bank.getOkienka().length;i++){
                new AwariaOkienka(bank,i,bank.getSimGenerator().exponential(bank.getOtoczenie().getAwaria()));
            }

            new PrzybycieKlienta(bank,null,0);
            new PrzybycieKlienta(bank,null,0.01);
            new PrzybycieKlienta(bank,null,0.02);

            simManager.setSimTimeRatio(spowolnienie/100);
            SimManager.simMode = SimParameters.SimMode.ASTRONOMICAL;

        } catch (SimControlException ex) {
            ex.printStackTrace();
        }
    }

    public static void stop(){
        simManager.stopSimulation();
    }
    public static void changeSpeed(double spowolnienie){
        System.out.println(spowolnienie);
        simManager.setSimTimeRatio(spowolnienie);
    }

    @Override
    public void run() {
        try {
            simManager.startSimulation();
        } catch (SimControlException e) {
            e.printStackTrace();
        }
        statystyki();
    }

    private void statystyki() {
        Diagram dg = new Diagram(Diagram.DiagramType.HISTOGRAM, "ilosc klientow w odziale (histogram)");
        dg.add(bank.iloscWOddziale, Color.cyan);
        dg.show();
        System.out.println("STATYSTYKI");
        System.out.println("oczekiwaną graniczną liczbę klientów w oddziale");
        System.out.println(Statistics.arithmeticMean(bank.iloscWOddziale));
        System.out.println("oczekiwaną graniczną liczbę klientów w kolejce");
        System.out.println(Statistics.arithmeticMean(bank.iloscWKolejce));
        System.out.println("oczekiwany graniczny czas obsługi klienta");
        System.out.println(Statistics.arithmeticMean(bank.czasDoObslugi));
        System.out.println("oczekiwany graniczny czas oczekiwania klienta w kolejce na rozpoczęcie obsługi");
        System.out.println(Statistics.arithmeticMean(bank.czasOczekiwaniaWKolejce));
        System.out.println("graniczne prawdopodobieństwo rezygnacji z obsługi przez klienta");
        System.out.println((double)bank.getStrataZniecierpliwienia()/bank.getMax_klientow());
    }
}
