package res;

import obiekty.Klient;

public class SortedLinkedList {

    class Node {
        private Node prev;
        private Node next;
        public Klient klient;

        private Node(Klient klient) {
            this.klient = klient;
        }
    }

    private Node head;
    private int size = 0;

    public void insert(Klient klient) {
        Node newNode = new Node(klient);
        this.size++;
        if (this.head == null) {
            this.head = newNode;
            head.prev = null;
            this.head.next = null;
        } else {
            Node curNode = this.head;
            if (curNode.klient.getPriorytet() > newNode.klient.getPriorytet()) {
                head.prev = newNode;
                newNode.next = head;
                newNode.prev = null;
                head = newNode;
                return;
            }
            while (curNode.next != null) {
                //System.out.println("xdd");
                if (curNode.klient.getPriorytet() <= newNode.klient.getPriorytet()) {
                    curNode = curNode.next;
                } else {
                    Node tempNode = curNode.prev;
                    newNode.next = curNode;
                    tempNode.next = newNode;
                    newNode.prev = tempNode;
                    curNode.prev = newNode;
                    return;
                }
            }
            newNode.prev = curNode;
            newNode.next = null;
            curNode.next = newNode;
        }

    }

    public Klient get(int index) {
        Node curNode = this.head;
        for (int i = 0; i < index; i++) {
            curNode = curNode.next;
            if (curNode == null) {
                System.out.println("Error w gecie");
                return null;
            }
        }
        return curNode.klient;
    }


    public Klient getAndRemoveFirst() {
        Node first = head;
        head = head.next;
        size--;
        return first.klient;

    }

    public int getSize() {
        return this.size;
    }


    public void remove(Klient klient) {
        Node curNode = this.head;

        if (curNode.klient.equals(klient)) {
            Node nextNode = curNode.next;
            if (nextNode == null) {
                head = null;
                size--;
                return;
            } else {
                nextNode.prev = null;
                head = nextNode;
                size--;
                return;
            }
        }
        while (curNode.next != null) {
            //System.out.println("xd");
            if (curNode.klient.equals(klient)) {
                Node privNode = curNode.prev;
                Node nextNode = curNode.next;

                privNode.next = nextNode;
                nextNode.prev = privNode;
                size--;
                return;
            }
            curNode = curNode.next;
        }
        if (curNode.klient.equals(klient)) {
            Node privNode = curNode.prev;
            privNode.next = null;
            size--;
            return;
        }


        System.out.println("Error");
    }

    public void print() {
        Node curNode = this.head;
        while (curNode != null) {
            System.out.println(curNode.klient.getPriorytet() + "  nr " + curNode.klient.getId());
            curNode = curNode.next;
        }
    }


}