package graphics;

import obiekty.Otoczenie;
import res.Symulacja;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SettingsFrame extends JFrame implements ActionListener, Runnable {

    private JSlider[] sliders = new JSlider[10];
    private JLabel[] labels = new JLabel[10];

    public SettingsFrame() {
        this.setTitle("Settings");
        createView();
        pack();
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(3);
        this.setVisible(true);
    }

    private void createView() {
        JButton jButton = new JButton("Rozpocznij symulacje");
        JPanel pOptions = new JPanel();
        this.add(pOptions);

        JPanel pOptionLabels = new JPanel(new GridBagLayout());
        pOptions.add(pOptionLabels);

        GridBagConstraints gbc = new GridBagConstraints();

        labels[0] = new JLabel("ilosc okienek");
        labels[1] = new JLabel("Ilosc priorytetow");
        labels[2] = new JLabel("Długość obsługi");
        labels[3] = new JLabel("Naprawa");
        labels[4] = new JLabel("Przyjscie klienta");
        labels[5] = new JLabel("Awaria");
        labels[6] = new JLabel("Zniecierpliwienie");
        labels[7] = new JLabel("pojemnosc kolejki");
        labels[8] = new JLabel("Długość symulacji");
        labels[9] = new JLabel("Szybkość symulacji");

        int RD_MIN_VALUE = 1;
        int RD_MAX_VALUE = 10;
        sliders[0] = new JSlider(RD_MIN_VALUE, RD_MAX_VALUE, 6);
        sliders[0].setMinorTickSpacing(1);
        sliders[0].setMajorTickSpacing(5);

        sliders[1] = new JSlider(RD_MIN_VALUE, RD_MAX_VALUE, 3);
        sliders[1].setMinorTickSpacing(1);
        sliders[1].setMajorTickSpacing(5);

        sliders[2] = new JSlider(RD_MIN_VALUE, RD_MAX_VALUE, 2);
        sliders[2].setMinorTickSpacing(1);
        sliders[2].setMajorTickSpacing(5);

        sliders[3] = new JSlider(RD_MIN_VALUE, RD_MAX_VALUE, 1);
        sliders[3].setMinorTickSpacing(1);
        sliders[3].setMajorTickSpacing(5);

        sliders[4] = new JSlider(RD_MIN_VALUE, RD_MAX_VALUE, 1);
        sliders[4].setMinorTickSpacing(1);
        sliders[4].setMajorTickSpacing(5);

        sliders[5] = new JSlider(0, 40, 10);
        sliders[5].setMinorTickSpacing(4);
        sliders[5].setMajorTickSpacing(20);

        sliders[6] = new JSlider(0, 40, 5);
        sliders[6].setMinorTickSpacing(4);
        sliders[6].setMajorTickSpacing(20);

        sliders[7] = new JSlider(0, 100, 50);
        sliders[7].setMinorTickSpacing(10);
        sliders[7].setMajorTickSpacing(50);

        sliders[8] = new JSlider(10, 250, 100);
        sliders[8].setMinorTickSpacing(24);
        sliders[8].setMajorTickSpacing(120);

        sliders[9] = new JSlider(0, 200, 30);
        sliders[9].setMinorTickSpacing(20);
        sliders[9].setMajorTickSpacing(100);

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.insets = new Insets(2, 2, 2, 2);

        for (JLabel j : labels) {
            pOptionLabels.add(j, gbc);
            gbc.gridy++;
        }
        gbc.gridx++;
        gbc.gridy = 0;
        for (JSlider slider : sliders) {
            slider.setPaintTicks(true);
            slider.setPaintLabels(true);
            pOptionLabels.add(slider, gbc);
            gbc.gridy++;
        }
        gbc.gridy++;

        jButton.addActionListener(this);
        pOptionLabels.add(jButton, gbc);
    }


    private int tylkoRaz = 0;

    @Override
    public void actionPerformed(ActionEvent e) {
        for (int i = 0; i < sliders.length - 1; i++) {
            sliders[i].setEnabled(false);
        }

        if (tylkoRaz == 0) {
            tylkoRaz = 1;
            Otoczenie otoczenie = new Otoczenie(sliders[0].getValue(), sliders[1].getValue(), sliders[2].getValue(), sliders[3].getValue(), sliders[4].getValue(),
                    sliders[5].getValue(), sliders[6].getValue(), sliders[7].getValue(), sliders[8].getValue());
            Symulacja symulacja = new Symulacja();
            symulacja.start(otoczenie, sliders[9].getValue());
            Thread thread = new Thread(symulacja);
            thread.start();
        } else {
            System.out.println(sliders[9].getValue());
            Symulacja.changeSpeed((double) sliders[9].getValue() / 100);
        }
    }
    @Override
    public void run() {
    }
}