package graphics;

import obiekty.Bank;
import obiekty.Klient;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class Screen extends JPanel implements Runnable {
    private int width;
    private int height;
    private Thread thread;
    private boolean running = false;
    private BufferedImage img;
    private Graphics2D g;
    private Bank bank;
    private Color[] colors;

    public Screen(Bank bank, int width, int height) {

        Color[] allColors = new Color[11];
        allColors[0] = new Color(255, 0, 0);
        allColors[1] = new Color(0, 240, 0);
        allColors[2] = new Color(0, 150, 0);
        allColors[3] = new Color(0, 70, 0);
        allColors[4] = new Color(0, 0, 240);
        allColors[5] = new Color(0, 0, 150);
        allColors[6] = new Color(0, 0, 70);
        allColors[7] = new Color(255, 255, 0);
        allColors[8] = new Color(150, 150, 0);
        allColors[9] = new Color(70, 70, 0);
        allColors[10] = new Color(40, 40, 0);

        this.width = width;
        this.height = height;
        this.bank = bank;
        setPreferredSize(new Dimension(width, height));
        setFocusable(true);
        requestFocus();
        colors = new Color[bank.getOtoczenie().getIlosc_priorytetow() + 1];
        System.arraycopy(allColors, 0, colors, 0, colors.length);
    }


    public void addNotify() {
        super.addNotify();
        if (thread == null) {
            thread = new Thread(this, "GameThread");
            thread.start();
        }
    }

    public void init() {
        running = true;
        img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        g = (Graphics2D) img.getGraphics();
    }

    public void run() {
        init();
        while (running) {
            render();
            draw();
        }
    }

    private void render() {
        Klient klient;
        if (g != null) {
            g.setColor(new Color(33, 30, 39));
            g.fillRect(0, 0, width, height);
            //okienka
            for (int i = 0; i < bank.getOtoczenie().getIlosc_okienek(); i++) {
                if (bank.getOkienka()[i].isAwaria()) {
                    g.setColor(new Color(220, 40, 40));
                } else if (bank.getOkienka()[i].isWolne()) {
                    g.setColor(new Color(0, 221, 0));
                } else {
                    g.setColor(new Color(220, 220, 40));
                }
                g.fillRect(10 + i * 150, 100, 50, 50);

                if ((klient = bank.getOkienka()[i].getKlient()) != null) {
                    g.setColor(colors[klient.getPriorytet()]);
                    g.fillOval(85 + i * 150, 100, 50, 50);
                    g.drawString(Long.toString(klient.getId()), 85 + i * 150, 100);
                }
            }

            //kolejka
            int y = 400;
            int x = 0;
            for (int i = 0; i < bank.getKolejka().getSize(); i++) {
                x += 100;
                if (x + 100 > 1300) {
                    y += 100;
                    x = 100;
                }
                Klient klient1;
                if ((klient1 = (bank.getKolejka().get(i))) != null) {
                    g.setColor(colors[klient1.getPriorytet()]);
                    g.fillOval(x, y, 50, 50);
                    g.drawString(Long.toString(klient1.getId()), x, y);
                }
            }
            g.setColor(new Color(255, 255, 255));
            g.drawString("Zniecierpliwienie : " + bank.getStrataZniecierpliwienia(), 1350, 700);
            g.drawString("Strata (kolejka) : " + bank.getStrata(), 1350, 750);
        }
    }

    private void draw() {
        Graphics g2 = this.getGraphics();
        g2.drawImage(img, 0, 0, width, height, null);
        g2.dispose();
    }

}
