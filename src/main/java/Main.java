import dissimlab.simcore.SimControlException;
import graphics.SettingsFrame;


public class Main {

    public static void main(String[] args) throws SimControlException {
       Thread thread = new Thread(new SettingsFrame());
       thread.start();
    }
}
