package obiekty;
import lombok.Data;
import res.SortedLinkedList;

@Data
public class Kolejka {

    private SortedLinkedList kolejka = new SortedLinkedList();
    private int MaxSize;
    public Kolejka(int maxSize) {
        this.MaxSize = maxSize;
    }

    public boolean addClient(Klient client) {
        if (kolejka.getSize() < MaxSize) {
            kolejka.insert(client);
            return true;
        } else {
            return false;
        }
    }

    public Klient get(int i) {
        return kolejka.get(i);
    }

    public Klient getAndRemoveFirst() {
        return kolejka.getAndRemoveFirst();
    }

    public int getSize() {
        return kolejka.getSize();
    }

    public void remove(Klient klient) {
        kolejka.remove(klient);
    }

}