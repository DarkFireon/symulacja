package obiekty;

import lombok.Data;

@Data
public class Okienko {

    boolean wolne,awaria;
    Kolejka kolejka;
    int id;
    int strata;
    Klient klient;

    public Okienko(int id, Kolejka kolejka) {
        this.id = id;
        this.kolejka = kolejka;
        wolne = true;
    }

}
