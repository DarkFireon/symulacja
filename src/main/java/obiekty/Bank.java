package obiekty;

import dissimlab.monitors.MonitoredVar;
import dissimlab.random.SimGenerator;
import dissimlab.simcore.BasicSimObj;
import dissimlab.simcore.SimManager;
import lombok.Getter;
import lombok.Setter;
import wydarzenia.OpuszczenieOkienka;

@Getter
@Setter
public class Bank extends BasicSimObj {

    public MonitoredVar iloscWOddziale;
    public MonitoredVar iloscWKolejce;
    public MonitoredVar czasDoObslugi;
    public MonitoredVar czasOczekiwaniaWKolejce;

    SimGenerator simGenerator = new SimGenerator();
    Okienko[] okienka;
    int strata;
    Otoczenie otoczenie;
    int max_klientow;
    int ilosc_klientow;
    int strataZniecierpliwienia = 0;
    Kolejka kolejka;
    OpuszczenieOkienka[] opuszczeniaOkienka;

    public Bank(Otoczenie otoczenie, SimManager simManager) {
        this.otoczenie = otoczenie;
        opuszczeniaOkienka = new OpuszczenieOkienka[6];
        max_klientow = otoczenie.getMax_klientow();
        ilosc_klientow = 0;
        okienka = new Okienko[otoczenie.getIlosc_okienek()];
        kolejka = new Kolejka(otoczenie.getMaxKolejka());
        for (int i = 0; i < okienka.length; i++) {
            okienka[i] = new Okienko(i, kolejka);
        }
        czasOczekiwaniaWKolejce = new MonitoredVar(0,simManager);
        czasDoObslugi = new MonitoredVar(0,simManager);
        iloscWKolejce = new MonitoredVar(0,simManager);
        iloscWOddziale = new MonitoredVar(0,simManager);
    }

    public int getKlienciWOkienkach()
    {
        int klienciWOkienkach = 0;
        for(Okienko i : okienka)
        {
            if (!i.isWolne())
                klienciWOkienkach++;
        }
        return klienciWOkienkach;
    }
}
