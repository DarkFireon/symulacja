package obiekty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.beans.ConstructorProperties;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Otoczenie {
    private int ilosc_okienek = 6 ;
    private int ilosc_priorytetow=3;
    private double obsługaOkienka = 2;
    private double naprawa = 1;
    private double przyjscieKlienta = 1;

    private double awaria = 10;
    private double niecierpliwosc = 5;
    private int maxKolejka = 50;
    private int max_klientow = 100;
}
