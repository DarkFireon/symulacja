package obiekty;

import lombok.Getter;
import lombok.Setter;

import java.util.concurrent.atomic.AtomicLong;

@Getter
@Setter
public class Klient {
    private static AtomicLong idCounter = new AtomicLong();
    long id;
    boolean wyszedl = false;
    int priorytet;
    boolean inOkienko;
    int nrOkienka;
    double startCzekania;
    public double startObslugi;
    public Klient() {
        this.id = idCounter.getAndIncrement();
        inOkienko = false;
    }
}
