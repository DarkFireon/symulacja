package wydarzenia;

import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;
import obiekty.Bank;
import obiekty.Klient;
import obiekty.Okienko;

public class PrzybycieKlienta extends BasicSimEvent<Bank, Object> {
    private Klient klient;
    private boolean staryKlient = false;

    public PrzybycieKlienta(Bank entity, Klient klient, double delay) throws SimControlException {
        super(entity, delay);
        this.klient = klient;
    }

    protected void stateChange() throws SimControlException {
        Bank bank = getSimObj();
        if (klient == null) {
            klient = new Klient();
            new ZnieciepliwienieKlienta(bank, bank.getSimGenerator().exponential(bank.getOtoczenie().getNiecierpliwosc()), klient);
            klient.setPriorytet(Math.abs(bank.getSimGenerator().nextInt()) % bank.getOtoczenie().getIlosc_priorytetow() + 1);
            bank.setIlosc_klientow(bank.getIlosc_klientow() + 1);
            staryKlient = false;
        } else {
            staryKlient = true;
        }

        if (bank.getIlosc_klientow() == bank.getMax_klientow()) {
            System.out.println("----------Zamykamy----------");
            System.out.println("----------wszedł ostatni klient----------");
        }

        boolean wszedl = bank.getKolejka().addClient(klient);
        if (!wszedl) {
            bank.setStrata(bank.getStrata() + 1);
            System.out.println(simTime() + "::Przybyl klient nr: " + klient.getId()
                    + "klient nie wszedl do żadnej kolejki - strata zwiekszona:: strata na ten moment: " + bank.getStrata());
            klient.setWyszedl(true);
        } else {
            klient.setStartCzekania(simTime());
            bank.iloscWKolejce.setValue(bank.getKolejka().getSize());
            bank.iloscWOddziale.setValue(bank.getKlienciWOkienkach() + bank.getKolejka().getSize());
            System.out.println(simTime() + " :: Przybyl klient nr: " + klient.getId() + "z priorytetem " + klient.getPriorytet()
                    + "::klient dodany do kolejki  wielkosc kolejki na ten moment: " + bank.getKolejka().getSize());
        }
        for (Okienko okienko : bank.getOkienka()) {
            if (okienko.getKolejka().getSize() == 1 && okienko.isWolne()) {
                WejscieDoOkienka wejscieDoOkienka = new WejscieDoOkienka(bank, okienko, 0);
                wejscieDoOkienka.onTermination();
                break;
            }
        }
        if (!staryKlient) {
            if (bank.getMax_klientow() > bank.getIlosc_klientow()) {
                double dt = bank.getSimGenerator().exponential(bank.getOtoczenie().getPrzyjscieKlienta());
                new PrzybycieKlienta(bank, null, dt);
            }
        }
    }

    protected void onTermination() throws SimControlException {
    }

    protected void onInterruption() throws SimControlException {
    }
}
