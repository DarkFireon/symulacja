package wydarzenia;

import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;
import obiekty.Bank;
import obiekty.Klient;
import obiekty.Okienko;
import res.Symulacja;

public class ZnieciepliwienieKlienta extends BasicSimEvent<Bank, Klient> {
    private Klient klient;

    ZnieciepliwienieKlienta(Bank entity, double delay, Klient klient) throws SimControlException {
        super(entity, delay, klient);
        this.klient = klient;
    }

    @Override
    protected void stateChange() throws SimControlException {
        Bank bank = getSimObj();

        if (klient.isWyszedl()) {
            return;
        }
        bank.setStrataZniecierpliwienia(bank.getStrataZniecierpliwienia() + 1);
        if (klient.isInOkienko()) {
            bank.iloscWOddziale.setValue(bank.getKlienciWOkienkach() + bank.getKolejka().getSize());
            System.out.println("###############" + simTime() + " klient " + klient.getId() + " zrezygnował bedąc w okienku " + klient.getNrOkienka());
            bank.getOkienka()[klient.getNrOkienka()].setKlient(null);
            bank.getOkienka()[klient.getNrOkienka()].setWolne(true);
            klient.setWyszedl(true);
            bank.getOpuszczeniaOkienka()[klient.getNrOkienka()].terminate();
            bank.getOpuszczeniaOkienka()[klient.getNrOkienka()] = null;

            if (bank.getMax_klientow() <= bank.getIlosc_klientow()) {
                int i = 0;
                for (Okienko o : bank.getOkienka()) {
                    if (o.isWolne() || o.isAwaria()) {
                        i++;
                    }
                }
                if (i == bank.getOkienka().length) {
                    Symulacja.stop();
                }
            }
        } else {
            bank.iloscWOddziale.setValue(bank.getKlienciWOkienkach() + bank.getKolejka().getSize());
            bank.iloscWKolejce.setValue(bank.getKolejka().getSize());
            bank.getKolejka().remove(klient);
            System.out.println("klient " + klient.getId() + " zniecierpliwil sie, obecna strata znicierpliwienia: " + bank.getStrataZniecierpliwienia());
        }
    }

    @Override
    protected void onTermination() throws SimControlException {
    }

    @Override
    protected void onInterruption() throws SimControlException {
    }
}
