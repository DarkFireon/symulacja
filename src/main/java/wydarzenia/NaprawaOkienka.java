package wydarzenia;

import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;
import obiekty.Bank;

public class NaprawaOkienka extends BasicSimEvent<Bank, Object> {
    private int okienko;

    NaprawaOkienka(Bank entity, int okienko, double delay) throws SimControlException {
        super(entity, delay);
        this.okienko = okienko;
    }

    @Override
    protected void stateChange() throws SimControlException {
        Bank bank = getSimObj();
        System.out.println(simTime() + "!!!!!!!!!!Okienko naprawione nr " + okienko);
        bank.getOkienka()[okienko].setWolne(true);
        bank.getOkienka()[okienko].setAwaria(false);
        if (bank.getOkienka()[okienko].getKolejka().getSize() > 0 && bank.getOkienka()[okienko].isWolne()) {
            new WejscieDoOkienka(bank, bank.getOkienka()[okienko], 0);
        }

        if (bank.getMax_klientow() > bank.getIlosc_klientow()) {
            double delay = bank.getSimGenerator().exponential(bank.getOtoczenie().getAwaria());
            new AwariaOkienka(bank, okienko, delay);
        }
    }
    @Override
    protected void onTermination() throws SimControlException {
    }

    @Override
    protected void onInterruption() throws SimControlException {
    }

}
