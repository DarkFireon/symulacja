package wydarzenia;

import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;
import obiekty.Bank;
import obiekty.Klient;
import obiekty.Okienko;
import res.Symulacja;

public class OpuszczenieOkienka extends BasicSimEvent<Bank, Object> {
    private Klient klient;
    private Okienko okienko;
    private Bank bank;

    OpuszczenieOkienka(Bank entity, Klient klient, Okienko okienko, double delay) throws SimControlException {
        super(entity, delay);
        this.klient = klient;
        this.okienko = okienko;
    }

    @Override
    protected void stateChange() throws SimControlException {
        bank = getSimObj();
        okienko.setKlient(null);
        System.out.println(simTime() + " ::Klient " + klient.getId() + " wychodzi z okienka " + okienko.getId());
        bank.czasDoObslugi.setValue(simTime() - klient.startObslugi);
        klient.setInOkienko(false);
        if (Math.abs(bank.getSimGenerator().nextInt()) % 10 == 0) {
            System.out.println("--------------Klient " + klient.getId() + " wchodzi z powrotem do kolejki ------------ ");
            new PrzybycieKlienta(bank, klient, 0);
        }else{
            bank.iloscWOddziale.setValue(bank.getKlienciWOkienkach() + bank.getKolejka().getSize());
        }
        klient.setWyszedl(true);
        klient = null;
        okienko.setWolne(true);
        if (okienko.getKolejka().getSize() > 0) {
            new WejscieDoOkienka(bank, okienko, 0);
        }
        if (bank.getMax_klientow() <= bank.getIlosc_klientow()) {
            int i=0;
            for(Okienko o : bank.getOkienka()){
                if(o.isWolne() || o.isAwaria()){
                    i++;
                }
            }
            if(i==bank.getOkienka().length){
                Symulacja.stop();
            }
        }
    }
    @Override
    protected void onTermination() throws SimControlException {
        bank = getSimObj();
        okienko.setKlient(null);
        if (okienko.getKolejka().getSize() > 0 && okienko.isWolne()) {
            new WejscieDoOkienka(bank, okienko, 0);
        }
    }
    @Override
    protected void onInterruption() throws SimControlException {
    }

    public Klient getKlient() {
        return klient;
    }
}
