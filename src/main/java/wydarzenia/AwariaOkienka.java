package wydarzenia;

import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;
import obiekty.Bank;
import obiekty.Klient;

public class AwariaOkienka extends BasicSimEvent<Bank, Object> {
    private int idOkienka;

    public AwariaOkienka(Bank entity, int okienko, double delay) throws SimControlException {
        super(entity, delay);
        this.idOkienka = okienko;
    }

    @Override
    protected void stateChange() throws SimControlException {
        Bank bank = getSimObj();
        System.out.println("!!!!!!!!!!AWARIA OKIENKA nr " + idOkienka);
        bank.getOkienka()[idOkienka].setWolne(false);
        bank.getOkienka()[idOkienka].setAwaria(true);
        if (bank.getOpuszczeniaOkienka()[idOkienka] != null) {
            bank.getOpuszczeniaOkienka()[idOkienka].onInterruption();
            Klient klient = bank.getOpuszczeniaOkienka()[idOkienka].getKlient();
            if (klient != null) {
                bank.getOpuszczeniaOkienka()[idOkienka].terminate();
                klient.setPriorytet(0);
                klient.setInOkienko(false);
                new PrzybycieKlienta(bank, klient, 0);
                System.out.println(simTime() + " klient " + klient.getId() + " przerzucony do kolejki");
            }
        }
        double dt = bank.getSimGenerator().exponential(bank.getOtoczenie().getNaprawa());
        new NaprawaOkienka(bank, idOkienka, dt);
    }

    @Override
    protected void onTermination() throws SimControlException {
    }
    @Override
    protected void onInterruption() throws SimControlException {
    }
}
