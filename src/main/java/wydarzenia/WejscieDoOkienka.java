package wydarzenia;

import dissimlab.simcore.BasicSimEvent;
import dissimlab.simcore.SimControlException;
import obiekty.Bank;
import obiekty.Klient;
import obiekty.Okienko;

public class WejscieDoOkienka extends BasicSimEvent<Bank, Object> {
    private Okienko okienko;

    WejscieDoOkienka(Bank entity, Okienko okienko, double delay) throws SimControlException {
        super(entity, delay);
        this.okienko = okienko;
    }

    @Override
    protected void stateChange() throws SimControlException {
        okienko.setWolne(false);
        Bank bank = getSimObj();
        Klient klient = null;
        if (bank.getKolejka().getSize() > 0) {
            klient = okienko.getKolejka().getAndRemoveFirst();
            bank.czasOczekiwaniaWKolejce.setValue(simTime() - klient.getStartCzekania());
            klient.startObslugi = simTime();
            bank.iloscWKolejce.setValue(bank.getKolejka().getSize());
        } else {
            return;
        }
        klient.setNrOkienka(okienko.getId());
        klient.setInOkienko(true);
        okienko.setKlient(klient);
        System.out.println(simTime() + " ::Klient nr " + klient.getId() + " wszedł do okienka nr " + okienko.getId() + " wielko kolejki to " + bank.getKolejka().getSize());

        double delay = bank.getSimGenerator().exponential(bank.getOtoczenie().getObsługaOkienka());
        OpuszczenieOkienka opuszczenieOkienka = new OpuszczenieOkienka(bank, klient, okienko, delay);
        bank.getOpuszczeniaOkienka()[okienko.getId()] = opuszczenieOkienka;
    }

    @Override
    protected void onTermination() throws SimControlException {
    }

    @Override
    protected void onInterruption() throws SimControlException {
    }
}
